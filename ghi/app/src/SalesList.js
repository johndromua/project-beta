import React, { useEffect, useState } from "react";


function SalesList() {
  const [sales, setSales] = useState([])

  const getData = async () => {
      const response = await fetch('http://localhost:8090/api/sales');

      if (response.ok) {
          const data = await response.json();
          setSales(data.sales);
      }
  };

  useEffect(() => {
      getData();
  }, []);

  return (
    <div>
      <h1>Sales</h1>
      <table className='table table-striped'>
        <thead>
          <tr>
            <th>Salesperson Employee ID</th>
            <th>Salesperson Name</th>
            <th>Customer</th>
            <th>VIN</th>
            <th>Price</th>
          </tr>
        </thead>
        <tbody>
          {sales.map((sale, index) => (
            <tr key={index}>
              <td>{sale.automobile ? sale.automobile.vin : "N/A"}</td>
              <td>{sale.salesperson ? `${sale.salesperson.employee_id}` : "N/A"}</td>
              <td>{sale.salesperson ? `${sale.salesperson.first_name} ${sale.salesperson.last_name}` : "N/A"}</td>
              <td>{sale.customer ? `${sale.customer.first_name} ${sale.customer.last_name}` : "N/A"}</td>
              <td>{sale.price}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

export default SalesList;
