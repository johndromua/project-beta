import { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom'

const TechnicianList = () => {
    const [technicians, setTechnicians] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8080/api/technicians/');

        if (response.ok) {
            const data = await response.json();
            setTechnicians(data.technicians)
        }
    }

    useEffect(() => {
        getData();
    }, []);

    return (
        <>
            <div className="row text-center mt-3">
                <h1 className="mb-2">Technicians</h1>
                    <p>
                        <NavLink to="/technicians/form" className="btn btn-primary btn-lg px-4 gap-3">Add a Technician</NavLink>
                    </p>
            </div>
            <table className ='table table-striped'>
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First name</th>
                        <th>Last name</th>
                    </tr>
                </thead>
                <tbody>
                    {technicians.map(tech => {
                        return (
                            <tr key={ tech.id }>
                                <td>{ tech.employee_id }</td>
                                <td>{ tech.first_name }</td>
                                <td>{ tech.last_name }</td>
                                {/* <td>
                                    <button className="btn btn-danger" onClick={handleDelete} value={ technician.id }>Remove</button>
                                </td> */}
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </>
    )
}

export default TechnicianList;
