import { NavLink } from 'react-router-dom';
import { useState, useEffect } from 'react';

const AppointmentHistory = () => {
    const [appointments, setAppointments] = useState([]);
    const [search, setSearch] = useState('');

    useEffect(() => {
        fetchAppointments();
    }, []);

    const fetchAppointments = async () => {
        try {
            const response = await fetch('http://localhost:8080/api/appointments/');
            if (response.ok) {
                const { appointments } = await response.json();
                const completeAppointments = appointments.filter(appointment => appointment.status !== 'Incomplete');
                setAppointments(completeAppointments);
            } else {
                console.error(response);
            }
        } catch (error) {
            console.error(error);
        }
    };

    const handleFormChange = (e) => {
        setSearch(e.target.value);
    };

    const handleClear = () => {
        setSearch('');
    };

    return (
        <>
            <div className="row text-center mt-3">
                <h1 className="mb-2">Service History</h1>
                <p>
                    <NavLink to="/appointments/form" className="btn btn-sm btn-primary">Schedule an Appointment</NavLink>
                </p>
                <p>
                    <NavLink to="/appointments" className="btn btn-sm btn-secondary">Service Appointments</NavLink>
                </p>
            </div>
            <div className="row align-items-center">
                <div className="col-2">
                    <div className="input-group">
                        <div className="form-floating">
                            <input
                                placeholder="Search VIN"
                                onChange={handleFormChange}
                                type="text"
                                name="search"
                                id="search"
                                className="form-control"
                                value={search}
                            />
                            <label htmlFor="search">Search VIN</label>
                        </div>
                    </div>
                </div>
                <div className="col">
                    <button className="btn btn-danger" onClick={handleClear}>
                        Clear
                    </button>
                </div>
            </div>
            <div className="row justify-content-center text-center">
                <div className="col">
                    <table className="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>VIN</th>
                                <th>VIP?</th>
                                <th>Customer</th>
                                <th>Date/Time</th>
                                <th>Technician</th>
                                <th>Reason</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            {appointments.map((appointment) => (
                                (appointment.vin === search || search === '') && (
                                    <tr key={appointment.id}>
                                        <td>{appointment.vin}</td>
                                        <td>{appointment.vip ? 'Yes' : 'No'}</td>
                                        <td>{appointment.customer}</td>
                                        <td>{appointment.date_time}</td>
                                        <td>{appointment.technician}</td>
                                        <td>{appointment.reason}</td>
                                        <td>{appointment.status}</td>
                                    </tr>
                                )
                            ))}
                        </tbody>
                    </table>
                </div>
            </div>
        </>
    );
};

export default AppointmentHistory;
