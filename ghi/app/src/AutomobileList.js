import { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom'

const AutomobileList = () => {
    const [autos, setAutomobiles] = useState([]);

    const getData = async () => {
        const response = await fetch('http://localhost:8100/api/automobiles/');

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos)
        }
    }
    useEffect(() => {
        getData();
    }, []);

    return (
        <>
            <div className="row text-center mt-3">
                <h1 className="mb-2">Automobiles</h1>
                    <p>
                        <NavLink to="/automobiles/form" className="btn btn-primary btn-lg px-4 gap-3">Add an Automobile</NavLink>
                    </p>
            </div>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {autos.map(auto => {
                        return (
                            <tr key={ auto.vin }>
                                <td>{ auto.vin }</td>
                                <td>{ auto.color }</td>
                                <td>{ auto.year }</td>
                                <td>{ auto.model.name }</td>
                                <td>{ auto.model.manufacturer.name }</td>
                                <td>{ auto.sold ? "Yes":"No" }</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    )
}
export default AutomobileList;
