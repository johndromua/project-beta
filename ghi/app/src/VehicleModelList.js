import { useState, useEffect } from "react";
import { NavLink } from 'react-router-dom';

const VehicleModelList = () => {
    const [models, setModels] = useState([]);

    async function getModels() {
        const response = await fetch('http://localhost:8100/api/models/');

        if (response.ok) {
            const { models } = await response.json();
            setModels(models);
        } else {
            console.error(response);
        }
    }

    useEffect(() => {
        getModels()
    }, []);

    const handleDelete = async (event) => {
        event.preventDefault();
        const model_id = event.target.value;
        const url = `http://localhost:8100/api/models/${model_id}`;
        const fetchConfig = {
            method: "delete",
            headers: {'Content-Type': 'application/json'}
        };

        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            getModels();
        }
    };

    return (
        <>
            <div className="row text-center mt-3">
                <h1 className="mb-2">Models</h1>
                    <p>
                        <NavLink to="/models/form" className="btn btn-primary btn-lg px-4 gap-3">Create a Model</NavLink>
                    </p>
            </div>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={ model.id }>
                                <td>{ model.name }</td>
                                <td>{ model.manufacturer.name }</td>
                                <td>
                                    <img src={ model.picture_url } alt='' width="30%" height="30%" />
                                </td>
                                <td>
                                    <button className="btn btn-danger" onClick={handleDelete} value={ model.id }>Delete Model</button>
                                </td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default VehicleModelList;
