import React, { useEffect, useState } from "react";

function SalespeopleList() {
    const [salespeoples, setSalespeople] = useState([])

    const getData = async () => {
        const response = await fetch('http://localhost:8090/api/salespeople/')

        if(response.ok) {
            const data = await response.json();
            setSalespeople(data.salespeople)
        }
    }
    useEffect(() => {
        getData();
    }, []);
    return (
        <div>
            <table className ='table table-striped'>
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Empoloyee ID</th>
                    </tr>
                    </thead>
                    <tbody>
                        {salespeoples.map(salespeople => {
                            return (
                                <tr key={salespeople.employee_id}>
                                    <td>{ salespeople.first_name }</td>
                                    <td>{ salespeople.last_name }</td>
                                    <td>{ salespeople.employee_id }</td>
                                </tr>
                            )
                        })}
                    </tbody>
            </table>
        </div>
    )
}

export default SalespeopleList;
