import React, { useState, useEffect } from 'react';

function AutomobileForm() {
    const [models, setModels] = useState([]);
    const [formData, setFormData] = useState({
        color: '',
        year: '',
        vin: '',
        model_id: '',
    });
    const getData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if(response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    };
    useEffect(() => {
        getData();
    }, []);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8100/api/automobiles/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setFormData({
                color: '',
                year: '',
                vin: '',
                model_id: ''
            });
        }
    };

    const handleFormChange = (e) => {
        const value = e.target.value;
        const inputName = e.target.name;

        setFormData({
            ...formData,
            [inputName]: value
        });
    };

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add an Automobile to inventory</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.color}
                placeholder="color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color" style={{ opacity: 0.5 }}>Color...</label>
            </div>
            <div className="form-floating mb-3">
              <label htmlFor="year"></label>
              <input
                onChange={handleFormChange}
                value={formData.year}
                placeholder="Year"
                name="year"
                id="year"
                className="form-control"
              />
              <label htmlFor="year" style={{ opacity: 0.5 }}>Year...</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleFormChange}
                value={formData.vin}
                placeholder="vin"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="vin" style={{ opacity: 0.5 }}>Vin...</label>
            </div>
            <div className="mb-3">
              <select
                onChange={handleFormChange}
                value={formData.model}
                required
                name="model_id"
                id="model"
                className="form-select"
              >
                <option value="">Choose a model</option>
                {models.map((model) => (
                  <option key={model.href} value={model.id}>
                    {model.name}
                  </option>
                ))}
              </select>
            </div>
            <button type="submit" className="btn btn-primary">
              Create
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AutomobileForm;
