import React, { useEffect, useState } from "react";

function SalespersonHistory() {
  const [salespeople, setSalespeople] = useState([]);
  const [selectedSalesperson, setSelectedSalesperson] = useState();
  const [sales, setSales] = useState([]);

  const getData = async () => {
    try {
      const salespeopleUrl = "http://localhost:8090/api/salespeople/";
      const salesUrl = "http://localhost:8090/api/sales/";

      const salespeopleResponse = await fetch(salespeopleUrl);

      if (salespeopleResponse.ok) {
        const salespeopleData = await salespeopleResponse.json();
        setSalespeople(salespeopleData.salespeople);
      }

      const salesResponse = await fetch(salesUrl);
      if (salesResponse.ok) {
        const salesData = await salesResponse.json();
        setSales(salesData.sales);

      }
    } catch (error) {
      console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  const handleSalespersonChange = (event) => {
    const selectedSalespersonId = event.target.value;
    setSelectedSalesperson(selectedSalespersonId);
  };

  const filteredSales = selectedSalesperson
    ? sales.filter((sale) => sale.salesperson.employee_id === parseInt(selectedSalesperson))
    : sales;


    return (
        <div>
          <h1>Salesperson History</h1>
          <select
            onChange={handleSalespersonChange}
            value={selectedSalesperson}
            name="salesperson"
            id="salesperson"
            className="form-select"
          >
            <option value="">Choose a salesperson...</option>
            {salespeople.map((salesperson) => (
              <option key={salesperson.employee_id} value={salesperson.employee_id}>
                {`${salesperson.first_name} ${salesperson.last_name}`}
              </option>
            ))}
          </select>
          <table className="table table-striped">
            <thead>
              <tr>
                <th>Salesperson</th>
                <th>Customer</th>
                <th>VIN</th>
                <th>Price</th>
              </tr>
            </thead>
            <tbody>
              {filteredSales.map((sale, id) => (
                <tr key={id}>
                  <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                  <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                  <td>{sale.automobile.vin}</td>
                  <td>{sale.price}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      );
}

    export default SalespersonHistory;
