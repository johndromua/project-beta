import { NavLink } from 'react-router-dom';

function Nav() {
  return (

    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">

            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/models">Models</NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/automobiles">Automobiles</NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/technicians">Technicians</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/technicians/form">Add a Technician</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/salespeople/form">Add a Salesperson</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/salespeople/history">Salesperson History</NavLink>
            </li>

            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/appointments">Service Appointments</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/appointments/form">Schedule an Appointment</NavLink>
            </li>
            <li className="nav-item">
              <NavLink className="nav-link active" aria-current="page" to="/appointments/history">Service History</NavLink>
            </li>

          </ul>
          <div className="container-fluid">
              <NavLink className="navbar-brand" aria-current="page" to="/salespeople">Salespeople</NavLink>
          </div>

          <div className="container-fluid">
              <NavLink className="navbar-brand" to="/manufacturers">Manufacterers</NavLink>
          </div>
          <div className="nav-item">
              <NavLink className="navbar-brand" to="/manufacturers/form">Create a Manufacterer</NavLink>
          </div>

          <div className="nav-item">
            <NavLink className="navbar-brand" to="/customers">Customers</NavLink>
          </div>
          <div className="nav-item">
            <NavLink className="navbar-brand" to="/customers/form">Add a customer</NavLink>
          </div>
          <div className="nav-item">
            <NavLink className="navbar-brand" to="/sales/form">Add a Sale</NavLink>
          </div>
          <div className="nav-item">
            <NavLink className="navbar-brand" to="/sales">Sales</NavLink>
          </div>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
