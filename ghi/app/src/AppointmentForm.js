import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function AppointmentForm() {
    const navigate = useNavigate();

    const [formData, setFormData] = useState({
        vin: '',
        customer: '',
        date_time: '',
        technician: '',
        reason: ''
    });
    const [techs, setTechs] = useState([]);

    async function getTechs() {
        try {
            const response = await fetch('http://localhost:8080/api/technicians/');
            if (response.ok) {
                const data = await response.json();
                setTechs(data.technicians);
            } else {
                console.error(response);
            }
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        getTechs();
    }, [])

    const handleSubmit = async (event) => {
        event.preventDefault();

        const url = 'http://localhost:8080/api/appointments/';

        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        try {
            const response = await fetch(url, fetchConfig);

            if (response.ok) {
                setFormData({
                    vin: '',
                    customer: '',
                    date_time: '',
                    technician: '',
                    reason: '',
                });
                navigate('/appointments');
            } else {
                console.error(response);
            }
        } catch (error) {
            console.error(error);
        }
    };

    const handleFormChange = (e) => {
        const { value, name } = e.target;

        setFormData({
            ...formData,
            [name]: value,
        });
    };

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Schedule an Appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">

                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Automobile VIN..." required type="text" name="vin" id="vin" maxLength='17' className="form-control" value={formData.vin} />
                            <label htmlFor="vin">Automobile VIN...</label>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Customer..." required type="text" name="customer" id="customer" className="form-control" value={formData.customer} />
                            <label htmlFor="customer">Customer...</label>
                        </div>

                        <div className='form-floating mb-3'>
                            <input onChange={handleFormChange}
                            placeholder="Date / Time" required type="datetime-local" name="date_time" id="date_time" className="form-control" value={formData.date_time}/>
                            <label htmlFor="date_time">Date / Time</label>
                        </div>

                        <div className="mb-3">
                            <select onChange={handleFormChange} placeholder="Choose a technician..." required name="technician" id="technician" className="form-select" value={formData.technician}>
                                <option value="">Choose a technician...</option>
                                {techs.map(tech => {
                                    return (
                                        <option key={tech.id} value={tech.employee_id}>{tech.first_name} {tech.last_name}</option>
                                    )
                                })}
                            </select>
                        </div>

                        <div className="form-floating mb-3">
                            <input onChange={handleFormChange} placeholder="Reason..." required type="text" name="reason" id="reason" className="form-control" value={formData.reason} />
                            <label htmlFor="reason">Reason...</label>
                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default AppointmentForm;
