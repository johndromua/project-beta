import React, { useState, useEffect } from 'react'

function SaleForm() {
    const [autos, setAutos] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [sales, setSales] = useState([]);
    const [formData, setFormData] = useState({
        automobile: '',
        salesperson: '',
        customer: '',
        price: 0,
    });



    const getData = async () => {
      try {
        const automobileUrl = 'http://localhost:8100/api/automobiles/'
        const salespeopleUrl = "http://localhost:8090/api/salespeople/"
        const customerUrl = "http://localhost:8090/api/customers/"
        const saleUrl = "http://localhost:8090/api/sales/"

        const autoResponse = await fetch(automobileUrl);

        if(autoResponse.ok) {
            const automobileData = await autoResponse.json();
            const unsoldAutos = automobileData.autos.filter(auto => !auto.sold);
            setAutos(unsoldAutos);
        }
        const salespeopleResponse = await fetch(salespeopleUrl);
        if(salespeopleResponse.ok) {
            const salespeopleData = await salespeopleResponse.json();
            setSalespeople(salespeopleData.salespeople);
        }
        const customerResponse = await fetch(customerUrl);
        if(customerResponse.ok) {
            const customerData = await customerResponse.json();
            setCustomers(customerData.customers)
        }
        const saleResponse = await fetch(saleUrl);
        if(saleResponse.ok) {
            const salesData = await saleResponse.json();
            setSales(salesData.sales)
        }
    } catch (error) {
        console.error('Error fetching data:', error);
    }
  };

  useEffect(() => {
    getData();
  }, []);


  const handleSubmit = async (event) => {
    event.preventDefault();

    const url = `http://localhost:8090/api/sales/`;

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(url, fetchConfig);

    if (response.ok) {
      setFormData({
        automobile: '',
        salesperson: '',
        customer: '',
        price: 0,
      });
    }
  };

  const handleFormChange = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  };


  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Record a new sale</h1>
          <form onSubmit={handleSubmit} id="create-conference-form">
            <div className="mb-3">
            <label htmlFor="vin">Automobile VIN</label>
            <select
                onChange={handleFormChange}
                value={formData.automobile}
                name="automobile"
                id="automobile"
                className="form-select"
            >
            <option value="">Choose an automobile VIN...</option>
                {autos.map((auto) => (
                <option key={auto.vin} value={auto.vin}>{auto.vin}</option>
                ))}
            </select>
            </div>

            <div className="mb-3">
            <label htmlFor="salesperson">Salesperson</label>
            <select
                onChange={handleFormChange}
                value={formData.salesperson}
                name="salesperson"
                id="salesperson"
                className="form-select"
            >
            <option value="">Choose a salesperson...</option>
            {salespeople.map((salesperson) => (
                <option key={salesperson.employee_id} value={`${salesperson.employee_id}`}>
                {salesperson.first_name} {salesperson.last_name}
                </option>
            ))}
            </select>
            </div>

            <label htmlFor='customer'>Customer</label>
            <select
                onChange={handleFormChange}
                value={formData.customer}
                name="customer"
                id="customer"
                className="form-select"
            >
            <option value="">Choose a customer...</option>
            {customers.map((customer) => (
                <option
                key={customer.phone_number}
                value={`${customer.phone_number}`}
                >
                    {customer.first_name} {customer.last_name}
                </option>
            ))}
            </select>

            <label htmlFor='price'>Price</label>
            <input
                onChange={handleFormChange}
                value={formData.price}
                type="number"
                name="price"
                id="price"
                className="form-control"
            />
            <div>
        </div>
            <button type="submit" className="btn btn-primary">
              Create
            </button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default SaleForm;
