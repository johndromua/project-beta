import { useState, useEffect } from 'react';
import { NavLink } from 'react-router-dom';

const AppointmentList = () => {
    const [appointments, setAppointments] = useState([]);

    useEffect(() => {
        const getAppointments = async () => {
            try {
                const response = await fetch('http://localhost:8080/api/appointments/');
                if (response.ok) {
                    const data = await response.json();
                    setAppointments(data.appointments);
                } else {
                    console.error(response);
                }
            } catch (error) {
                console.error(error);
            }
        };
        getAppointments();
    }, []);

    const handleFinish = async (id) => {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${id}/finish/`, {
                method: 'PUT',
            });

            if (response.ok) {
                setAppointments(prevAppointments =>
                    prevAppointments.filter(appointment => appointment.id !== id)
                );
            } else {
                console.error(response);
            }
        } catch (error) {
            console.error(error);
        }
    };

    const handleCancel = async (id) => {
        try {
            const response = await fetch(`http://localhost:8080/api/appointments/${id}/cancel/`, {
                method: 'PUT',
            });

            if (response.ok) {
                setAppointments(prevAppointments =>
                    prevAppointments.filter(appointment => appointment.id !== id)
                );
            } else {
                console.error(response);
            }
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <>
            <div className="row text-center mt-3">
                <h1 className="mb-2">Service Appointments</h1>
                    <p>
                        <NavLink to="/appointments/form" className="btn btn-primary btn-lg px-4 gap-3">Schedule an appointment</NavLink>
                    </p>
                    <p>
                        <NavLink to="/appointments/history" className="btn btn-sm btn-secondary">Service History</NavLink>
                    </p>
            </div>
            <table className='table table-striped'>
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>VIP?</th>
                        <th>Customer</th>
                        <th>Date/Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments.map(appointment => (
                        <tr key={appointment.id}>
                            <td>{appointment.vin}</td>
                            <td>{appointment.vip ? 'Yes' : 'No'}</td>
                            <td>{appointment.customer}</td>
                            <td>{appointment.date_time}</td>
                            <td>{appointment.technician}</td>
                            <td>{appointment.reason}</td>
                            <td>{appointment.status}</td>
                            <td>
                                {appointment.status === 'pending' && (
                                    <>
                                        <button className="btn btn-primary" onClick={() => handleFinish(appointment.id)}>Finish</button>
                                        <button className="btn btn-danger" onClick={() => handleCancel(appointment.id)}>Cancel</button>
                                    </>
                                )}
                            </td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </>
    )
}

export default AppointmentList;
