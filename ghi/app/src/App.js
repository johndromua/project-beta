import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ManufacturerList from './ManufacturerList';
import ManufactererForm from './ManufacturerForm';
import Nav from './Nav';
import VehicleModelList from './VehicleModelList';
import VehicleModelForm from './VehicleModelForm';
import AutomobileList from './AutomobileList';
import AutomobileForm from './AutomobileForm';
import TechnicianForm from './TechnicianForm';
import TechnicianList from './TechnicianList';
import AppointmentForm from './AppointmentForm';
import AppointmentList from './AppointmentList';
import AppointmentHistory from './AppointmentHistory';
import SalespeopleList from './SalespeopleList';
import SalespersonForm from './SalespersonForm';
import SalespersonHistory from './SalespersonHistory';
import CustomerList from './CustomerList';
import CustomerForm from './CustomerForm';
import SalesForm from './SalesForm';
import SalesList from './SalesList';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />

          <Route path="models">
            <Route index element={<VehicleModelList />} />
            <Route path="form" element={<VehicleModelForm />} />
          </Route>

          <Route path="automobiles">
            <Route index element={<AutomobileList />} />
            <Route path="form" element={<AutomobileForm />} />
          </Route>

          <Route path="manufacturers" element={<ManufacturerList />} />

          <Route path="technicians">
            <Route index element={<TechnicianList />} />
            <Route path="form" element={<TechnicianForm />} />
          </Route>

          <Route path="appointments">
            <Route index element={<AppointmentList />} />
            <Route path="form" element={<AppointmentForm />} />
            <Route path="history" element={<AppointmentHistory />} />
          </Route>

          <Route path="manufacturers">
            <Route index element={<ManufacturerList />} />
            <Route path="form" element={<ManufactererForm />} />
          </Route>


          <Route path="customers">
            <Route index element ={<CustomerList />} />
            <Route path="form" element={<CustomerForm />} />
          </Route>

          <Route path="sales">
            <Route index element={<SalesList /> } />
            <Route path="form" element={<SalesForm />} />
          </Route>

          <Route path="salespeople">
            <Route index element={<SalespeopleList />} />
            <Route path="form" element={<SalespersonForm /> } />
            <Route path="history" element={<SalespersonHistory /> } />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
