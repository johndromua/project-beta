from django.db import models
from django.urls import reverse


# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=50, primary_key=True)
    sold = models.BooleanField(default=False)


class Salesperson(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.IntegerField(primary_key=True)


class Customer(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    address = models.CharField(max_length=220)
    phone_number = models.CharField(max_length=20, primary_key=True)

    def __str__(self):
        return self.phone_number

    def get_api_url(self):
        return reverse("api_customer", kwargs={"pk": self.pk})


class Sale(models.Model):
    price = models.IntegerField()

    automobile = models.ForeignKey(
        AutomobileVO, related_name="sales", on_delete=models.CASCADE, null=True
    )
    salesperson = models.ForeignKey(
        Salesperson, related_name="sales", on_delete=models.CASCADE, null=True
    )
    customer = models.ForeignKey(
        Customer, related_name="sales", on_delete=models.CASCADE, null=True
    )

    def __str__(self):
        return self.automobile.vin

    def get_api_url(self):
        return reverse("api_sale", kwargs={"pk": self.pk})
