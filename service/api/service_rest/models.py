from django.db import models

# Create your models here.
class Technician(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return f"{self.first_name} {self.last_name} ({self.employee_id})"

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin

class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.TextField()
    STATUS_OPTIONS = [
        ("pending", "Pending"),
        ("finished", "Finished"),
        ("canceled", "Canceled"),
    ]
    status = models.CharField(
        max_length=10,
        choices=STATUS_OPTIONS,
        default="pending"
    )
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=50)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE
    )
    vip = models.BooleanField(default=False)
    class Meta:
        ordering = ["date_time"]
