from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from .models import Technician, Appointment, AutomobileVO
from common.json import ModelEncoder
import json

# Create your views here.

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id",
    ]

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vip",
        "date_time",
        "reason",
        "status",
        "vin",
        "customer",
        "technician",
    ]
    def get_extra_data(self, o):
        return {
            "technician": f"{o.technician.first_name} {o.technician.last_name}",
        }
    # encoders = {
    #     "technician": TechnicianListEncoder(),
    # }

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianListEncoder,
            safe=False,
        )
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False
            )
        except Technician.DoesNotExist:
            response = JsonResponse(
                {"message": "Error adding Technician"}
            )
            response.status_code = 400
            return response

@require_http_methods(["DELETE"])
def api_delete_technician(request, pk):
    try:
        count, _ = Technician.objects.get(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    except Technician.DoesNotExist:
        response = JsonResponse(
            {"message": "Error deleting Technician"}
        )
        response.status_code = 400
        return response

@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.all()
            return JsonResponse(
                {"appointments": appointments},
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse(
                {"message": "Error listing appointments"}
            )
            response.status_code = 400
            return response
    elif request.method == "POST":
        try:
            content = json.loads(request.body)
            try:
                technician_id = content["technician"]
                technician = Technician.objects.get(employee_id=technician_id)
                content["technician"] = technician
            except Technician.DoesNotExist:
                response = JsonResponse(
                    {"message": "Error finding Technician"}
                )
                response.status_code = 400
                return response

            # check VIN history
            check_vin = AutomobileVO.objects.filter(vin=content["vin"])
            if check_vin:
                content["vip"] = True
            else:
                content["vip"] = False

            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False
            )
        except:
            response = JsonResponse(
                {"message": "Error scheduling an Appointment"}
            )
            response.status_code = 400
            return response

@require_http_methods("DELETE")
def api_delete_appointment(request, pk):
    try:
        count, _ = Appointment.objects.get(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}
        )
    except Appointment.DoesNotExist:
        response = JsonResponse(
            {"message": "Error deleting Appointment"}
        )
        response.status_code = 400
        return response

@require_http_methods(["PUT"])
def api_cancel_appointment(request, pk):
    try:
        Appointment.objects.filter(id=pk).update(status="canceled")
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )
    except Appointment.DoesNotExist:
        response = JsonResponse(
            {"message": "Error canceling Appointment"}
        )
        response.status_code = 400
        return response

@require_http_methods(["PUT"])
def api_finish_appointment(request, pk):
    try:
        Appointment.objects.filter(id=pk).update(status="finished")
        appointment = Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False
        )
    except Appointment.DoesNotExist:
        response = JsonResponse(
            {"message": "Error finishing Appointment"}
        )
        response.status_code = 400
        return response
