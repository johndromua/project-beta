# CarCar

Team:


* John Romua - Services Microservice
* Tyler Valentine- sales microservice


## Design
CarCar is an application designed to address the needs of car dealerships by providing efficient data management and streamlined processes. The application is composed of multiple microservices, a database, and a front-end React library, all working together to create an interactive and user-friendly experience. The features found in this application include: Sales management, Service management, Inventory tracking, and many more! Read Microservice details down below.

## Diagram
![img](CarCarDiagram.png)

## Getting Started
* Prerequisites - Make sure Docker is installed and running

1. Fork the repository

2. Clone the forked repository by running this command in the terminal:
    git clone <<repository.url.here>>

3. Build Docker images with these commands:
    docker volume create beta-data
    docker-compose build
    docker-compose up

4. Check to make sure your containers are running

5. View the project in the browser: http://localhost:3000/

## Service microservice
* DESCRIPTION:
The Services Microservice is a system responsible for managing technicians and service appointments within the services system. It handles the creation, deletion, and updates to employed technicians, as well as the scheduling and tracking of service appointments. The microservice keeps track of both upcoming and historical appointments, providing detailed information about the customer, appointment date/time, VIP status (determined by the VIN from a vehicle sold from the inventory), the reason for the appointment, the assigned technician, and the status of the appointment (pending, finish, canceled).

* MODELS
    - Technician
    Properties:
        1. first_name
        2. last_name
        3. employee_id

    - Appointment
    Properties:
        1. date_time
        2. reason
        3. status
        4. vin
        5. vip
        6. technician

    - AutomobileVO (Value Object)
    Properties:
        1. vin
        2. sold

* PORTS | URL PATHS | CRUD OPERATIONS:

| Method               | URL Path                             | Action                                      |
|----------------------|--------------------------------------|---------------------------------------------|
| Service Microservice | `http://localhost:8080/api/`         |                                             |
| GET, POST            | `technicians/`                       | Get a list of all technicians.              |
| DELETE               | `technicians/<int:pk>/`              | Delete a specific technician by ID.         |
| GET, POST            | `appointments/`                      | Get a list of all appointments.             |
| DELETE               | `appointments/<int:pk>/`             | Delete specific appointment by ID.          |
| PUT                  | `appointments/<int:pk>/cancel/`      | Set appointment status to "canceled".       |
| PUT                  | `appointments/<int:pk>/finish/`      | Set appointment status to "finished".       |



## Sales microservice

The Sales microservice is in charge of the creation and listing of salespeople, customers and sales. The sales model handles the creation of sales records based on information it pulls from salespeople, customer, and automobile models. It also keeps track of what automobiles are sold and unsold.

Salespeople MODEL:
List salespeople models (GET)
http://localhost:8090/api/salespeople/
Create a salesperson model (POST)
http://localhost:8090/api/salespeople/

Return value of listing all salespeople:
```
{
	"salespeople": [
		{
			"first_name": "Tyler",
			"last_name": "Valentine",
			"employee_id": 1
		},
		{
			"first_name": "John",
			"last_name": "Romua",
			"employee_id": 2
		}
	]
}
```
To create a new salesperson:
```
JSON(body):
{
  "first_name": "Tyler",
  "last_name": "Valentine",
  "employee_id": "1"
}
```
Return value of creating a new salesperson:
```
{
  "first_name": "Tyler",
  "last_name": "Valentine",
  "employee_id": "1"
}
```


Customer MODEL:
List customer models (GET)
http://localhost:8090/api/customers/
Create a customer model (POST)
http://localhost:8090/api/customers/
``
Return value of listing all customers:
{
	"customers": [
		{
			"href": "/api/customers/13123123",
			"first_name": "Elrond",
			"last_name": "Elve",
			"address": "125 Fake Street, Fakey, FA, 12357",
			"phone_number": "131-231-2345"
		},
		{
			"first_name": "Chucks",
			"last_name": "Berry",
			"address": "123 Fake Strteet, Fakey, FA, 12345",
			"phone_number": "123-456-7851"
		}
	]
}
```
To create a customer:
JSON(body):
```
{
	"first_name": "Chucks",
	"last_name": "Berrsy",
	"address": "123 Fake Strteet, Fakey, FA, 12345",
	"phone_number": "123-456-7801"
}
```
Return value of a customer:
```
{
	"first_name": "Chucks",
	"last_name": "Berrsy",
	"address": "123 Fake Strteet, Fakey, FA, 12345",
	"phone_number": "123-456-7801"
}
```

Sale MODEL:
List sale models (GET):
http://localhost:8090/api/sales/
Create a sale model (POST):
http://localhost:8090/api/sales/
```
Return value of listing all sales:
** automobile is the value object
{
	"sales": [
		{
			"href": "/api/sales/1",
			"id": 1,
			"automobile": {
				"vin": "F2133ASJF2244",
				"sold": false
			},
			"salesperson": {
				"first_name": "Tyler",
				"last_name": "Valentine",
				"employee_id": 1
			},
			"customer": {
				"first_name": "Chucks",
				"last_name": "Berry",
				"address": "123 Fake Strteet, Fakey, FA, 12345",
				"phone_number": "123-456-7851"
			},
			"price": 20,123
		}
	]
}
```
To create a sale model:
```
JSON(body):
```
	{
		"automobile": "QWIJDAKMND2231",
		"salesperson": 1,
		"customer": "123-456-7851",
		"price": 23299.00
	}
```
Return value of creating a sale model:
```
{
	"message": "Sale created successfully"
}
```

Value Objects:
Sales service - AutomobileVO model that retrives the vin and sold status from the inventory microservice's Automobile model.
